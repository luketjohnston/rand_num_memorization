
-- require 'vocab'
function enc2dec(s)
  encoder:evaluate()
  decoder:evaluate()
  
  -- find next sequence
  input_seq = {}
  for i=1,#s do 
    input = torch.ByteTensor({vocab.char_to_int[s:sub(i,i)]})
    if opt.useCuda == 1 then input = input:cuda() end
    table.insert(input_seq, input)
  end
    
  -- zero LSTM hidden state and CEC state
  local rnn_state = {}
  for L=1,num_layers do
    local h_init = torch.zeros(1,hiddenSize)
    if opt.useCuda == 1 then h_init = h_init:cuda() end
    table.insert(rnn_state, h_init:clone()) -- one for h
    table.insert(rnn_state, h_init:clone()) -- one for c
  end

  -- forward sequence through encoder
  for i=1,#input_seq do 
    rnn_state  = encoder:forward{input_seq[i], unpack(rnn_state)}
    -- remove softmax output, which isn't needed for next input
    table.remove(rnn_state,#rnn_state)
  end

  -- now forward through decoder, using same state
  --prediction = torch.ByteTensor({vocab.char_to_int[s:sub(1,1)]})
  prediction = torch.ByteTensor({vocab.end_char_ind})
  decoding = '' .. vocab.int_to_char[prediction[1]]

  repeat
    rnn_state = decoder:forward{prediction, unpack(rnn_state)}
    log_probs = rnn_state[#rnn_state]

    -- dividing by smaller temperature before exponentiating will emphasize
    -- more confident predictions
    log_probs:div(opt.temperature)
    local probs = torch.exp(log_probs):squeeze()
    probs:div(torch.sum(probs)) -- renormalize so probs sum to one. Is this necessary? not for multinomial...
    -- sample from probability distribution
    prediction = torch.multinomial(probs:float(), 1):resize(1)
    decoding = decoding .. vocab.int_to_char[prediction[1]]

    -- remove softmax output from state - it isn't needed for next input
    table.remove(rnn_state, #rnn_state)
  until prediction[1] == vocab.end_char_ind

  return decoding
end

  
