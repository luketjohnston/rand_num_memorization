require 'torch'   -- torch
require 'xlua'    -- xlua provides useful tools, like progress bars
require 'optim'   -- an optimization package, for online and batch methods

----------------------------------------------------------------------
print '==> defining test procedure'
require 'rnn'
require 'nn'
if opt.useCuda == 1 then require 'cunn' end


-- test function
function validate()
  -- set model to evaluate mode (for modules that differ in training and testing, like Dropout)
  encoder:evaluate()
  decoder:evaluate()

  local loss = 0

  seq_len = min_len+3 -- use max seq_len for validation

  -- get batch size so that we use one batch for the whole validation data
  batch_size = (#data.val)[1]/seq_len
  inputs,targets = makebatch(data.val,data.val,1,seq_len,batch_size)

  -- initial rnn state will be all zeros. 
  rnn_state = {}
  for i=1,num_layers do
    local h_init = torch.zeros(batch_size,hiddenSize)
    if opt.useCuda == 1 then h_init = h_init:cuda() end
    table.insert(rnn_state,h_init:clone()) -- one for h
    table.insert(rnn_state,h_init:clone()) -- one for c
  end

  -- forward sequence through encoder
  for i=1,#inputs do 
    rnn_state  = encoder:forward{inputs[i], unpack(rnn_state)}
    -- remove softmax output, which isn't needed for next input
    rnn_state[#rnn_state] = nil 
  end

  dec_outs = {} -- will hold the softmax distributions over chars
  -- now forward through decoder, using same state
  for i=1,#targets-1 do 
    rnn_state = decoder:forward{targets[i], unpack(rnn_state)}
    -- save softmax char distribution in dec_outs
    -- clone nec or else duplicates - I don't understand exactly why
    table.insert(dec_outs, rnn_state[#rnn_state]:clone())
    -- remove softmax output from state - it's not needed 4 next input
    rnn_state[#rnn_state] = nil 
  end

  -- calculate loss
  for i=#targets-1,1,-1 do 
    loss = loss + criterion:forward(dec_outs[i],targets[i+1])
  end


  -- get average loss per char
  loss = loss / #inputs

  return loss 
end

