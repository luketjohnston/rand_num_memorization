-- Trains the encoder and decoder.
-- see 'minibatch.lua', 'eval_loss.lua', and 'sample.lua' for important methods.


require 'torch'   -- torch
require 'xlua'    -- xlua provides useful tools, like progress bars
require 'optim'   -- an optimization package, for online and batch methods
require 'nn'
require 'rnn'
model_utils = require 'model_utils'
if opt.useCuda == 1 then require 'cunn' end
-- Defines functions to create and use minibatches.
require 'minibatch.lua' 
-- Defines the function that evaluates the loss of the model on a minibatch
require 'eval_loss.lua'
-- contains function to sample from the model (generate text with model).
require 'sample'

----------------------------------------------------------------------
print '==> configuring optimizer'

optimState = {
   learningRate = opt.learningRate,
   --weightDecay = opt.weightDecay,
   --momentum = opt.momentum,
   alpha = 0.95
}
optimMethod = optim.rmsprop
--optimMethod = optim.sgd

----------------------------------------------------------------------
print '==> defining training procedure'


train_size = (#data.train)[1]


-- does one epoch on the training data
function train()

  -- epoch tracker
  epoch = epoch or 1


  -- local vars
  local time = sys.clock()

  -- set model to training mode (for modules that differ in training and testing, like Dropout)

  -- do one epoch
  print('==> doing epoch on training data:')
  print("==> online epoch # " .. epoch )

  -- t is the current character being processed.
  local t = 1
  while t <= train_size do

    sequence = (sequence or 0) + 1
    -- if necessary, calculate validation loss and save best model
    if sequence % opt.valStep == 0 then
      print('Calculating validation loss...')
      local loss = validate()
      print('Validation loss: ' .. loss)
      losses = losses or {}
      table.insert(losses, loss)
      if (not min_loss) or (loss < min_loss) then
        min_loss = loss
        print('==> This is the best validation loss so far. Saving...')
        torch.save(opt.save .. '/encoder.net',encoder)
      else 
        print("Decaying learning rate by " .. opt.learningDecay)
        optimState.learningRate = optimState.learningRate * opt.learningDecay
      end

      -- make a random string (of max length that we've learned so far)
      inputs,targets = makebatch(data.train,data.train,t,min_len+3,1)
      print('Encoding and decoding ' .. batch_to_string(inputs))
      print(enc2dec(batch_to_string(inputs)))

      encoder:training()
      decoder:training()

      if (min_loss < 0.01) then
        min_loss = nil
        min_len = min_len + 3
        print("Val less than 0.01, so increasing min_len to " .. min_len)
        -- make more encoder and decoder clones
        print("Cloning encoder and decoder (for longer sequences)")
        for i=1,3 do
          enc_clones[#enc_clones+1] = model_utils.clone(encoder)
          dec_clones[#dec_clones+1] = model_utils.clone(decoder)
        end
      end
      
      torch.save(opt.save .. '/validation_loss_list.t7',losses)
    end
  
    -- disp progress bar
    xlua.progress(t, train_size)

    -- randomly select sequence length for next batch TODO encapsulate in
    -- separate module
    seq_len = torch.random(min_len,min_len+3)
    inputs,targets = makebatch(data.train, data.train, t, seq_len, opt.batchSize)
    t = t + (seq_len * opt.batchSize)

    -- TODO do we still need these checks for seq_len? prob not
    if seq_len > 1 and seq_len < data.largest_seq then
       

      -- create closure that optim needs
      -- x is the parameters to update the model with for this batch. Returns
      -- the average loss, and the normalized gradParameters
      local optimClosure = function(x)
        -- Prepare model for next pass by updating parameters and zeroing
        -- gradients
        if x ~= parameters then -- I don't think this ever actually happens...
          parameters:copy(x)
        end
        gradParameters:zero()
        return evalBatch(criterion,inputs,targets)
      end

      -- optimize parameters vector on current mini-batch
      optimMethod(optimClosure, parameters, optimState)
    end
  end

  -- time taken
  time = sys.clock() - time
  time = time / train_size
  print("\n==> time to learn 1 sample = " .. (time*1000) .. 'ms')



end
